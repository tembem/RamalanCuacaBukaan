package gulajava.ramalancuaca.utilans;

import android.content.Context;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Locale;

import gulajava.ramalancuaca.R;

/**
 * Created by Gulajava Ministudio
 */
public class UtilanCuaca {


    public static String formatTemperature(Context context, String strtemperature) {

        String temperaturDerajat = "0";


        try {
            double doubleTemperatur = Double.valueOf(strtemperature);
            // Data stored in Celsius by default.
            //simbol derajat di belakang angka
            String suffix = "\u00B0";

            // For presentation, assume the user doesn't care about tenths of a degree.
            String formatTemperatur = context.getResources().getString(R.string.format_temperature);
            temperaturDerajat = String.format(formatTemperatur, doubleTemperatur);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return temperaturDerajat;
    }


    //KONVERSI WAKTU MILIDETIK UNIX KE BENTUK JAM HH:MM
    public static String konversiMsJam(long strMsTanggal) {

        String tanggalJam = "-";

        // perlu dikalikan dengan 1000 dulu agar angka nya bisa ke Epoch Unix Time
        // open weather map reports the date as a unix timestamp (seconds)
        // convert it to milliseconds to convert to a Date object

        try {
            DateTime dateTime = new DateTime(strMsTanggal);

            //formatter ke HH:mm
            DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("HH:mm");
            DateTimeFormatter dateTimeFormatterID = dateTimeFormatter.withLocale(new Locale("id"));

            tanggalJam = dateTime.toString(dateTimeFormatterID);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return tanggalJam;
    }

    //KONVERSI MS KE BENTUK NAMA HARI
    public static String konversiMsHari(long strMsTanggal) {

        String tanggalHari = "-";

        // perlu dikalikan dengan 1000 dulu agar angka nya bisa ke Epoch Unix Time
        // open weather map reports the date as a unix timestamp (seconds)
        // convert it to milliseconds to convert to a Date object

        try {
            DateTime dateTime = new DateTime(strMsTanggal);

            //formatter ke EEEE
            DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("EEEE");
            DateTimeFormatter dateTimeFormatterID = dateTimeFormatter.withLocale(new Locale("id"));

            tanggalHari = dateTime.toString(dateTimeFormatterID);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return tanggalHari;
    }


    //KONVERSI WAKTU MILIDETIK UNIX KE BENTUK TANGGAL BIASA
    public static String konversiMsTanggal(long strMsTanggal) {

        String tanggalBiasa = "-";

        // perlu dikalikan dengan 1000 dulu agar angka nya bisa ke Epoch Unix Time
        // open weather map reports the date as a unix timestamp (seconds)
        // convert it to milliseconds to convert to a Date object

        try {

            DateTime dateTime = new DateTime(strMsTanggal);

            //formatter ke EEEE, dd MMMM yyyy
            DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("EEEE, dd MMMM yyyy");
            DateTimeFormatter dateTimeFormatterID = dateTimeFormatter.withLocale(new Locale("id"));

            tanggalBiasa = dateTime.toString(dateTimeFormatterID);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return tanggalBiasa;
    }


    public static String getFormattedWind(Context context, double windSpeed, double degrees) {

//        int windFormat;
//        if (Utility.isMetric(context)) {
//        windFormat = R.string.format_wind_kmh;
//        } else {
//            windFormat = R.string.format_wind_mph;
//            windSpeed = .621371192237334f * windSpeed;
//        }

        // From wind direction in degrees, determine compass direction as a string (e.g NW)
        // You know what's fun, writing really long if/else statements with tons of possible
        // conditions.  Seriously, try it!
        int windFormat = R.string.format_wind_kmjam;

        String direction = "Unknown";
        if (degrees >= 337.5 || degrees < 22.5) {
            direction = "Utara"; //N
        } else if (degrees >= 22.5 && degrees < 67.5) {
            direction = "Timur Laut"; //NE
        } else if (degrees >= 67.5 && degrees < 112.5) {
            direction = "Timur"; //E
        } else if (degrees >= 112.5 && degrees < 157.5) {
            direction = "Tenggara"; //SE
        } else if (degrees >= 157.5 && degrees < 202.5) {
            direction = "Selatan"; //S
        } else if (degrees >= 202.5 && degrees < 247.5) {
            direction = "Barat Daya"; //SW
        } else if (degrees >= 247.5 && degrees < 292.5) {
            direction = "Barat"; //W
        } else if (degrees >= 292.5 && degrees < 337.5) {
            direction = "Barat Laut"; //NW
        }
        return String.format(context.getString(windFormat), windSpeed, direction);
    }

    /**
     * Helper method to provide the icon resource id according to the weather condition id returned
     * by the OpenWeatherMap call.
     *
     * @param strKodeCuaca from OpenWeatherMap API response
     * @return resource id for the corresponding icon. -1 if no relation is found.
     */
    public static int getGambarCuacaDariKodeKondisi(String strKodeCuaca) {

        try {

            int weatherId = Integer.valueOf(strKodeCuaca);

            // Based on weather code data found at:
            // http://bugs.openweathermap.org/projects/api/wiki/Weather_Condition_Codes
            if (weatherId >= 200 && weatherId <= 232) {
                return R.drawable.weather_storm_night;
            } else if (weatherId >= 300 && weatherId <= 321) {
                return R.drawable.weather_showers_day;
            } else if (weatherId >= 500 && weatherId <= 504) {
                return R.drawable.weather_showers_night;
            } else if (weatherId == 511) {
                return R.drawable.weather_snow_scattered_night;
            } else if (weatherId >= 520 && weatherId <= 531) {
                return R.drawable.weather_showers_night;
            } else if (weatherId >= 600 && weatherId <= 622) {
                return R.drawable.weather_snow_scattered_night;
            } else if (weatherId >= 701 && weatherId <= 761) {
                return R.drawable.weather_haze;
            } else if (weatherId == 761 || weatherId == 781) {
                return R.drawable.weather_storm_night;
            } else if (weatherId == 800) {
                return R.drawable.weather_clear;
            } else if (weatherId == 801) {
                return R.drawable.weather_few_clouds;
            } else if (weatherId >= 802 && weatherId <= 804) {
                return R.drawable.weather_clouds;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return R.drawable.weather_none_available;
    }


    //AMBIL STATUS CUACA BERDASARKAN KODE CUACA YANG DIDAPAT
    //DARI DATA JSON NYA
    public static String getStatusCuacaTeks(String strkodeCuaca) {

        String namaCuaca = "-";

        try {
            int kodeCuaca = Integer.valueOf(strkodeCuaca);

            // http://bugs.openweathermap.org/projects/api/wiki/Weather_Condition_Codes
            if (kodeCuaca >= 200 && kodeCuaca <= 232) {

                namaCuaca = "Hujan Badai";

            } else if (kodeCuaca >= 300 && kodeCuaca <= 321) {

                namaCuaca = "Hujan Ringan";

            } else if (kodeCuaca >= 500 && kodeCuaca <= 504) {

                namaCuaca = "Hujan";

            } else if (kodeCuaca == 511) {

                namaCuaca = "Hujan Salju";

            } else if (kodeCuaca >= 520 && kodeCuaca <= 531) {

                namaCuaca = "Hujan";

            } else if (kodeCuaca >= 600 && kodeCuaca <= 622) {

                namaCuaca = "Hujan Salju";

            } else if (kodeCuaca >= 701 && kodeCuaca <= 761) {

                namaCuaca = "Berkabut";

            } else if (kodeCuaca == 761 || kodeCuaca == 781) {

                namaCuaca = "Hujan Badai";

            } else if (kodeCuaca == 800) {

                namaCuaca = "Cerah";

            } else if (kodeCuaca == 801) {

                namaCuaca = "Cerah Berawan";

            } else if (kodeCuaca >= 802 && kodeCuaca <= 804) {

                namaCuaca = "Berawan";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return namaCuaca;
    }


    public static int getStatusCuacaBackdrop(String strkodeCuaca) {

        int kodeGambar = R.drawable.backdrop_cerahawan;

        try {
            int kodeCuaca = Integer.valueOf(strkodeCuaca);

            // http://bugs.openweathermap.org/projects/api/wiki/Weather_Condition_Codes
            if (kodeCuaca >= 200 && kodeCuaca <= 232) {

                kodeGambar = R.drawable.backdrop_hujan;

            } else if (kodeCuaca >= 300 && kodeCuaca <= 321) {

                kodeGambar = R.drawable.backdrop_hujan;

            } else if (kodeCuaca >= 500 && kodeCuaca <= 504) {

                kodeGambar = R.drawable.backdrop_hujan;

            } else if (kodeCuaca == 511) {

                kodeGambar = R.drawable.backdrop_salju;

            } else if (kodeCuaca >= 520 && kodeCuaca <= 531) {

                kodeGambar = R.drawable.backdrop_hujan;

            } else if (kodeCuaca >= 600 && kodeCuaca <= 622) {

                kodeGambar = R.drawable.backdrop_salju;

            } else if (kodeCuaca >= 701 && kodeCuaca <= 761) {

                kodeGambar = R.drawable.backdrop_hujan;

            } else if (kodeCuaca == 761 || kodeCuaca == 781) {

                kodeGambar = R.drawable.backdrop_hujan;

            } else if (kodeCuaca == 800) {

                kodeGambar = R.drawable.backdrop_cerah;

            } else if (kodeCuaca == 801) {

                kodeGambar = R.drawable.backdrop_cerahawan;

            } else if (kodeCuaca >= 802 && kodeCuaca <= 804) {

                kodeGambar = R.drawable.backdrop_cerahawan;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return kodeGambar;
    }


    //AMBIL STATUS CUACA BERDASARKAN KODE CUACA YANG DIDAPAT
    //DARI DATA JSON NYA
    public static int getStatusCuacaNotifikasi(String strkodeCuaca) {

        int kodeAlamatGambar = R.drawable.ic_notifikasi_cuaca_cerah;

        // http://bugs.openweathermap.org/projects/api/wiki/Weather_Condition_Codes
        try {
            int kodeCuaca = Integer.valueOf(strkodeCuaca);

            if (kodeCuaca >= 200 && kodeCuaca <= 232) {

                //Hujan Badai
                kodeAlamatGambar = R.drawable.ic_notifikasi_cuaca_badai;

            } else if (kodeCuaca >= 300 && kodeCuaca <= 321) {

                //Hujan Ringan
                kodeAlamatGambar = R.drawable.ic_notifikasi_cuaca_hujan;

            } else if (kodeCuaca >= 500 && kodeCuaca <= 504) {

                //Hujan
                kodeAlamatGambar = R.drawable.ic_notifikasi_cuaca_hujan;

            } else if (kodeCuaca == 511) {

                //Hujan Salju
                kodeAlamatGambar = R.drawable.ic_notifikasi_cuaca_hujan;

            } else if (kodeCuaca >= 520 && kodeCuaca <= 531) {

                //Hujan
                kodeAlamatGambar = R.drawable.ic_notifikasi_cuaca_hujan;

            } else if (kodeCuaca >= 600 && kodeCuaca <= 622) {

                //Hujan Salju
                kodeAlamatGambar = R.drawable.ic_notifikasi_cuaca_hujan;

            } else if (kodeCuaca >= 701 && kodeCuaca <= 761) {

                //Berkabut
                kodeAlamatGambar = R.drawable.ic_notifikasi_cuaca_berawan;

            } else if (kodeCuaca == 761 || kodeCuaca == 781) {

                //Hujan Badai
                kodeAlamatGambar = R.drawable.ic_notifikasi_cuaca_badai;

            } else if (kodeCuaca == 800) {

                //Cerah
                kodeAlamatGambar = R.drawable.ic_notifikasi_cuaca_cerah;

            } else if (kodeCuaca == 801) {

                //Cerah Berawan
                kodeAlamatGambar = R.drawable.ic_notifikasi_cuaca_berawan;

            } else if (kodeCuaca >= 802 && kodeCuaca <= 804) {

                //Berawan
                kodeAlamatGambar = R.drawable.ic_notifikasi_cuaca_berawan;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return kodeAlamatGambar;
    }
}
