package gulajava.ramalancuaca.aktivitas;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.joda.time.DateTime;

import java.util.List;

import gulajava.ramalancuaca.R;
import gulajava.ramalancuaca.database.RMSetelan;
import gulajava.ramalancuaca.database.RealmKonfigurasi;
import gulajava.ramalancuaca.dialogs.DialogGagalLokasi;
import gulajava.ramalancuaca.dialogs.DialogGagalPermission;
import gulajava.ramalancuaca.internets.GoogleClientBuilder;
import gulajava.ramalancuaca.internets.StatusApp;
import gulajava.ramalancuaca.internets.sinkronisasi.RCuacaSyncAdapter;
import gulajava.ramalancuaca.utilans.CekGPSNet;
import gulajava.ramalancuaca.utilans.Konstan;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by Gulajava Ministudio.
 */
public class LoadingSplash extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {


    private Realm mRealm;
    private RealmQuery<RMSetelan> mSetelanRealmQuery;
    private RealmResults<RMSetelan> mSetelanRealmResults;
    private RMSetelan mRMSetelanKopi;

    private boolean isLokasiDapat = false;
    private boolean isSetelanAda = false;
    private String mStringLatitude;
    private String mStringLongitude;

    private CekGPSNet mCekGPSNet;
    private GoogleApiClient mGoogleApiClient = null;
    private Location mLocation;
    private LokasiListener mLokasiListenerNetwork;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.akt_splashscreen);

        mCekGPSNet = new CekGPSNet(LoadingSplash.this);
        RealmConfiguration realmConfiguration = RealmKonfigurasi.getKonfigurasiRealm(LoadingSplash.this);
        mRealm = Realm.getInstance(realmConfiguration);
        mLokasiListenerNetwork = new LokasiListener();

        //status aplikasi jalan, notifikasi buat jadi notifikasi kosong
        StatusApp.getInstance().setStatusAplikasiJalan(true);

    }


    @Override
    protected void onResume() {
        super.onResume();

        //cek setelan database ada isinya atau tidak
        //sudah ada lokasinya atau tidak
        cekSetelanDatabase();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mRealm.close();
        matikanGoogleApiClient();
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

        //tampil dialog peringatan butuh akses lokasi
        DialogGagalPermission dialogGagalPermission = new DialogGagalPermission();
        dialogGagalPermission.setCancelable(false);

        FragmentTransaction fragmentTransaction = LoadingSplash.this.getSupportFragmentManager().beginTransaction();
        dialogGagalPermission.show(fragmentTransaction, "GAGAL PERMISSION LOKASI");

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, LoadingSplash.this);
    }


    //CEK SETELAN DATABASE ADA ISI ATAU TIDAK
    private void cekSetelanDatabase() {

        mSetelanRealmQuery = mRealm.where(RMSetelan.class);
        mSetelanRealmResults = mSetelanRealmQuery.findAll();

        if (!mSetelanRealmResults.isEmpty()) {

            //sudah ada setelannya
            //perbarui lokasi
            //jalankan sinkronisasi
            //pindah ke halaman berikutnya
            isSetelanAda = true;
            mRMSetelanKopi = mRealm.copyFromRealm(mSetelanRealmResults.first());

            cekPermisiLokasi();

        } else {

            //belum ada setelan, inisialisasi awal
            //ambil lokasi pengguna awal
            isSetelanAda = false;

            cekPermisiLokasi();
        }
    }


    //CEK PERMISI LOKASI
    //UNTUK ANDROID 6.0 MARSHMALLOW DAN SETERUSNYA
    @AfterPermissionGranted(Konstan.ID_MINTALOKASI)
    private void cekPermisiLokasi() {

        if (EasyPermissions.hasPermissions(LoadingSplash.this, Konstan.PERMISSIONS)) {

            //sudah diijinkan minta lokasi
            //lanjut cek kondisi gps pengguna
            cekStatusGPS();

        } else {
            EasyPermissions.requestPermissions(LoadingSplash.this, LoadingSplash.this.getResources().getString(R.string.keterangan_rasional),
                    Konstan.ID_MINTALOKASI, Konstan.PERMISSIONS);
        }
    }


    //CEK KONDISI GPS PENGGUNA
    private void cekStatusGPS() {

        boolean isInternet = mCekGPSNet.cekStatusInternet();
        boolean isNetworkNyala = mCekGPSNet.cekStatusNetworkGSM();
        boolean isGPSNetwork = mCekGPSNet.cekStatusNetwork();

        //jika koneksi internet nyala , atau sinyal gsm nyala, dan ijin lokasi nyala
        if (isInternet && isGPSNetwork || isNetworkNyala && isGPSNetwork) {

            //minta lokasi dari google play service
            if (mGoogleApiClient == null) {

                mGoogleApiClient = GoogleClientBuilder.getLocationClient(LoadingSplash.this,
                        mConnectionCallbacksOk, mOnConnectionFailedListener);
            }

            //sambungkan ke google play service location
            jalanGoogleApiClient();

        } else {

            //gagal mengambil lokasi pengguna, tampil dialog minta lokasi dan keluar dari aplikasi
            DialogGagalLokasi dialogGagalLokasi = new DialogGagalLokasi();
            dialogGagalLokasi.setCancelable(false);

            FragmentTransaction fragmentTransaction = LoadingSplash.this.getSupportFragmentManager().beginTransaction();
            dialogGagalLokasi.show(fragmentTransaction, "GAGAL MINTA GPS MATI");
        }
    }


    //LISTENER LOKASI REQUEST
    protected class LokasiListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {

            if (location != null && !isLokasiDapat) {

                isLokasiDapat = true;
                mLocation = location;

                //simpan ke database dan inisialisasi setelan
                inisialisasiSimpanSetelan();
            }
        }
    }


    //INISIALISASI DATA SETELAN PENGGUNA
    //SIMPAN KE DATABASE
    private void inisialisasiSimpanSetelan() {

        final String strWaktuUpdateAwal = new DateTime().getMillis() + "";

        mStringLatitude = mLocation.getLatitude() + "";
        mStringLongitude = mLocation.getLongitude() + "";

        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                if (isSetelanAda) {

                    mRMSetelanKopi.setIdData(Konstan.ID_BARIS_SATU_DB);
                    mRMSetelanKopi.setStrLatitude(mStringLatitude);
                    mRMSetelanKopi.setStrLongitude(mStringLongitude);

                    realm.copyToRealmOrUpdate(mRMSetelanKopi);

                } else {

                    mRMSetelanKopi = new RMSetelan();

                    mRMSetelanKopi.setIdData(Konstan.ID_BARIS_SATU_DB);
                    mRMSetelanKopi.setStrMsWaktuUpdate(strWaktuUpdateAwal);
                    mRMSetelanKopi.setNamaLokasi("");
                    mRMSetelanKopi.setNamaLokasiPanjang("");
                    mRMSetelanKopi.setStrLatitude(mStringLatitude);
                    mRMSetelanKopi.setStrLongitude(mStringLongitude);
                    mRMSetelanKopi.setNotifikasiCuaca(true);

                    realm.copyToRealmOrUpdate(mRMSetelanKopi);
                }
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {

                //pindah ke halaman berikut
                inisialisasiSinkronsPindahHalaman();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {

                //gagal simpan keluar halaman aplikasi
                Toast.makeText(LoadingSplash.this, "Gagal inisialisasi data awal", Toast.LENGTH_SHORT).show();
                LoadingSplash.this.finish();
            }
        });
    }


    //CEK KONEKSI INTERNET
    //JALANKAN SINKRONISASI JIKA PERLU
    private void inisialisasiSinkronsPindahHalaman() {

        //sinkronisasi jika sebelumnya belum ada data di database, karena baru install
        if (!isSetelanAda) {
            RCuacaSyncAdapter.initializeSyncAdapter(LoadingSplash.this);
            RCuacaSyncAdapter.syncSegera(LoadingSplash.this);
        }

        Handler handler = new Handler();
        handler.postDelayed(mRunnableJedas, 1000);

    }

    Runnable mRunnableJedas = new Runnable() {
        @Override
        public void run() {
            Intent intent = new Intent(LoadingSplash.this, AktHalamanUtama.class);
            LoadingSplash.this.startActivity(intent);
            LoadingSplash.this.finish();
        }
    };


    /**
     * GOOGLE LOCATION CLIENT API
     * AMBIL LOKASI DARI GOOGLE PLAY SERVICE
     ***/


    //JALANKAN GOOGLE API CLIENT
    //MULAI API CLIENT
    public void jalanGoogleApiClient() {

        if (mGoogleApiClient != null && !mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
    }


    //MATIKAN API CLIENT LOKASI
    public void matikanGoogleApiClient() {

        if (mLokasiListenerNetwork != null && mGoogleApiClient != null) {
            try {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, mLokasiListenerNetwork);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }


    //LISTENER SAMBUNGAN OK KE GOOGLE PLAY SERVICE
    GoogleApiClient.ConnectionCallbacks mConnectionCallbacksOk = new GoogleApiClient.ConnectionCallbacks() {
        @Override
        public void onConnected(@Nullable Bundle bundle) {

            if (ActivityCompat.checkSelfPermission(LoadingSplash.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(LoadingSplash.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
            }

            mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            if (mLocation != null) {

                isLokasiDapat = true;
                //simpan ke database dan inisialisasi setelan
                inisialisasiSimpanSetelan();

            } else {

                isLokasiDapat = false;

                LocationRequest locationRequest = GoogleClientBuilder.getLocationGPSReq();
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, mLokasiListenerNetwork);
            }
        }

        @Override
        public void onConnectionSuspended(int i) {

            mGoogleApiClient.connect();
        }
    };


    //LISTENER SAMBUNGAN GAGAL KE GOOGLE PLAY SERVICE
    GoogleApiClient.OnConnectionFailedListener mOnConnectionFailedListener = new GoogleApiClient.OnConnectionFailedListener() {
        @Override
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

            //coba sambungan terus google api client
            if (!mGoogleApiClient.isConnecting() && !mGoogleApiClient.isConnected()) {
                mGoogleApiClient.connect();
            }
        }
    };

}
