package gulajava.ramalancuaca.modellogika;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.alibaba.fastjson.JSON;

import java.util.List;
import java.util.Locale;

import gulajava.ramalancuaca.internets.modelnet.cuacaperhari.CuacaPerHari;
import gulajava.ramalancuaca.internets.modelnet.cuacaperjam.CuacaPerJams;
import gulajava.ramalancuaca.models.MsgHasilGeocoder;


/**
 * Created by Gulajava Ministudio.
 */
public class Parsers {

    private Context mContext;

    //GEOCODER AMBIL LOKASI DAN POSISI PENGGUNA ALAMATNYA JIKA ADA
    private Geocoder geocoderPengguna;
    private List<Address> addressListPengguna = null;
    private String gecoder_alamat = "";
    private String gecoder_namakota = "";
    private String alamatgabungan = "";

    public Parsers(Context context) {
        mContext = context;
    }


    //AMBIL GEOCODER LOKASI
    public MsgHasilGeocoder ambilGeocoderPengguna(String latitude, String longitude) {

        geocoderPengguna = new Geocoder(mContext, Locale.ENGLISH);
        double dolatitu = 0;
        double dolongi = 0;

        try {
            dolatitu = Double.valueOf(latitude);
            dolongi = Double.valueOf(longitude);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {

            addressListPengguna = geocoderPengguna.getFromLocation(dolatitu, dolongi, 1);
            if (addressListPengguna.size() > 0) {

                int panjangalamat = addressListPengguna.get(0).getMaxAddressLineIndex();

                if (panjangalamat > 0) {

                    gecoder_alamat = addressListPengguna.get(0).getAddressLine(0);
                    gecoder_namakota = addressListPengguna.get(0).getLocality();

                } else {
                    gecoder_alamat = "";
                    gecoder_namakota = "";
                    alamatgabungan = "";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            gecoder_alamat = "";
            gecoder_namakota = "";
            alamatgabungan = "";
        }

        if (gecoder_namakota != null && gecoder_namakota.length() > 0) {

            alamatgabungan = gecoder_namakota;

            if (gecoder_alamat != null && gecoder_alamat.length() > 0) {

                alamatgabungan = gecoder_alamat + ", " + gecoder_namakota;

            }
        } else {
            alamatgabungan = "";
            gecoder_namakota = "";
        }

//        Log.w("ALAMAT GABUNGAN", "ALAMAT " + alamatgabungan + " gecoder kota " + gecoder_namakota);

        MsgHasilGeocoder msgHasilGeocoder = new MsgHasilGeocoder();
        msgHasilGeocoder.setAlamatgabungan(alamatgabungan);
        msgHasilGeocoder.setAlamatPendek(gecoder_namakota);

        return msgHasilGeocoder;
    }


    //DESERIALISASI JSON DATA RAMALAN CUACA PER HARI
    public CuacaPerHari parseJSONCuacaHarian(String stringJson) {

        CuacaPerHari cuacaPerHari = null;

        try {
            cuacaPerHari = JSON.parseObject(stringJson, CuacaPerHari.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return cuacaPerHari;
    }


    //DESERIALISASI JSON DATA RAMALAN CUACA PER JAM
    public CuacaPerJams parseJSONCuacaPerJAM(String stringJSON) {

        CuacaPerJams cuacaPerJams = null;

        try {
            cuacaPerJams = JSON.parseObject(stringJSON, CuacaPerJams.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return cuacaPerJams;
    }


}
