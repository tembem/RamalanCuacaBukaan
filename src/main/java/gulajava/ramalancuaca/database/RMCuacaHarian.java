package gulajava.ramalancuaca.database;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Gulajava Ministudio.
 */

public class RMCuacaHarian extends RealmObject {

    @PrimaryKey
    private int id_data;

    private long longwaktuMs;

//    private String waktuUpdatems;

    private String temperaturDay;
    private String temperaturMin;
    private String temperaturMax;
    private String temperaturNight;
    private String temperaturEve;
    private String temperaturMorn;

    private String tekananUdara;

    private String kelembabanUdara;

    private String idKodeCuaca;
    private String namaCuaca;
    private String keteranganCuaca;
    private String kodeIkon;

    private String kecepatanAngin;
    private String arahDerajatAngin;

    private String cuacaAwan;
    private String curahHujan;

    public RMCuacaHarian() {
    }

    public int getId_data() {
        return id_data;
    }

    public void setId_data(int id_data) {
        this.id_data = id_data;
    }

    public long getLongwaktuMs() {
        return longwaktuMs;
    }

    public void setLongwaktuMs(long longwaktuMs) {
        this.longwaktuMs = longwaktuMs;
    }

//    public String getWaktuUpdatems() {
//        return waktuUpdatems;
//    }
//
//    public void setWaktuUpdatems(String waktuUpdatems) {
//        this.waktuUpdatems = waktuUpdatems;
//    }

    public String getTemperaturDay() {
        return temperaturDay;
    }

    public void setTemperaturDay(String temperaturDay) {
        this.temperaturDay = temperaturDay;
    }

    public String getTemperaturMin() {
        return temperaturMin;
    }

    public void setTemperaturMin(String temperaturMin) {
        this.temperaturMin = temperaturMin;
    }

    public String getTemperaturMax() {
        return temperaturMax;
    }

    public void setTemperaturMax(String temperaturMax) {
        this.temperaturMax = temperaturMax;
    }

    public String getTemperaturNight() {
        return temperaturNight;
    }

    public void setTemperaturNight(String temperaturNight) {
        this.temperaturNight = temperaturNight;
    }

    public String getTemperaturEve() {
        return temperaturEve;
    }

    public void setTemperaturEve(String temperaturEve) {
        this.temperaturEve = temperaturEve;
    }

    public String getTemperaturMorn() {
        return temperaturMorn;
    }

    public void setTemperaturMorn(String temperaturMorn) {
        this.temperaturMorn = temperaturMorn;
    }

    public String getTekananUdara() {
        return tekananUdara;
    }

    public void setTekananUdara(String tekananUdara) {
        this.tekananUdara = tekananUdara;
    }

    public String getKelembabanUdara() {
        return kelembabanUdara;
    }

    public void setKelembabanUdara(String kelembabanUdara) {
        this.kelembabanUdara = kelembabanUdara;
    }

    public String getIdKodeCuaca() {
        return idKodeCuaca;
    }

    public void setIdKodeCuaca(String idKodeCuaca) {
        this.idKodeCuaca = idKodeCuaca;
    }

    public String getNamaCuaca() {
        return namaCuaca;
    }

    public void setNamaCuaca(String namaCuaca) {
        this.namaCuaca = namaCuaca;
    }

    public String getKeteranganCuaca() {
        return keteranganCuaca;
    }

    public void setKeteranganCuaca(String keteranganCuaca) {
        this.keteranganCuaca = keteranganCuaca;
    }

    public String getKodeIkon() {
        return kodeIkon;
    }

    public void setKodeIkon(String kodeIkon) {
        this.kodeIkon = kodeIkon;
    }

    public String getKecepatanAngin() {
        return kecepatanAngin;
    }

    public void setKecepatanAngin(String kecepatanAngin) {
        this.kecepatanAngin = kecepatanAngin;
    }

    public String getArahDerajatAngin() {
        return arahDerajatAngin;
    }

    public void setArahDerajatAngin(String arahDerajatAngin) {
        this.arahDerajatAngin = arahDerajatAngin;
    }

    public String getCuacaAwan() {
        return cuacaAwan;
    }

    public void setCuacaAwan(String cuacaAwan) {
        this.cuacaAwan = cuacaAwan;
    }

    public String getCurahHujan() {
        return curahHujan;
    }

    public void setCurahHujan(String curahHujan) {
        this.curahHujan = curahHujan;
    }
}
