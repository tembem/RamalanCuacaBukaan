package gulajava.ramalancuaca.database;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Gulajava Ministudio.
 */
public class RMCuacaPerJam extends RealmObject {

    @PrimaryKey
    private int id_data;

    private long waktuUpdateMs;

    private String temperatur;
    private String temperaturMin;
    private String temperaturMax;

    private String tekananUdara;

    private String ketinggianPermukaanLaut;
    private String ketinggianPermukaanTanah;
    private String kelembabanUdara;
    private String temperatureKelvin;

    private String idKodeCuaca;
    private String namaCuaca;
    private String keteranganCuaca;
    private String kodeIkon;

    private String kecepatanAngin;
    private String arahDerajatAngin;

    private String waktuUpdateString;

    public RMCuacaPerJam() {
    }

    public int getId_data() {
        return id_data;
    }

    public void setId_data(int id_data) {
        this.id_data = id_data;
    }

    public long getWaktuUpdateMs() {
        return waktuUpdateMs;
    }

    public void setWaktuUpdateMs(long waktuUpdateMs) {
        this.waktuUpdateMs = waktuUpdateMs;
    }

    public String getTemperatur() {
        return temperatur;
    }

    public void setTemperatur(String temperatur) {
        this.temperatur = temperatur;
    }

    public String getTemperaturMin() {
        return temperaturMin;
    }

    public void setTemperaturMin(String temperaturMin) {
        this.temperaturMin = temperaturMin;
    }

    public String getTemperaturMax() {
        return temperaturMax;
    }

    public void setTemperaturMax(String temperaturMax) {
        this.temperaturMax = temperaturMax;
    }

    public String getTekananUdara() {
        return tekananUdara;
    }

    public void setTekananUdara(String tekananUdara) {
        this.tekananUdara = tekananUdara;
    }

    public String getKetinggianPermukaanLaut() {
        return ketinggianPermukaanLaut;
    }

    public void setKetinggianPermukaanLaut(String ketinggianPermukaanLaut) {
        this.ketinggianPermukaanLaut = ketinggianPermukaanLaut;
    }

    public String getKetinggianPermukaanTanah() {
        return ketinggianPermukaanTanah;
    }

    public void setKetinggianPermukaanTanah(String ketinggianPermukaanTanah) {
        this.ketinggianPermukaanTanah = ketinggianPermukaanTanah;
    }

    public String getKelembabanUdara() {
        return kelembabanUdara;
    }

    public void setKelembabanUdara(String kelembabanUdara) {
        this.kelembabanUdara = kelembabanUdara;
    }

    public String getTemperatureKelvin() {
        return temperatureKelvin;
    }

    public void setTemperatureKelvin(String temperatureKelvin) {
        this.temperatureKelvin = temperatureKelvin;
    }

    public String getIdKodeCuaca() {
        return idKodeCuaca;
    }

    public void setIdKodeCuaca(String idKodeCuaca) {
        this.idKodeCuaca = idKodeCuaca;
    }

    public String getNamaCuaca() {
        return namaCuaca;
    }

    public void setNamaCuaca(String namaCuaca) {
        this.namaCuaca = namaCuaca;
    }

    public String getKeteranganCuaca() {
        return keteranganCuaca;
    }

    public void setKeteranganCuaca(String keteranganCuaca) {
        this.keteranganCuaca = keteranganCuaca;
    }

    public String getKodeIkon() {
        return kodeIkon;
    }

    public void setKodeIkon(String kodeIkon) {
        this.kodeIkon = kodeIkon;
    }

    public String getKecepatanAngin() {
        return kecepatanAngin;
    }

    public void setKecepatanAngin(String kecepatanAngin) {
        this.kecepatanAngin = kecepatanAngin;
    }

    public String getArahDerajatAngin() {
        return arahDerajatAngin;
    }

    public void setArahDerajatAngin(String arahDerajatAngin) {
        this.arahDerajatAngin = arahDerajatAngin;
    }

    public String getWaktuUpdateString() {
        return waktuUpdateString;
    }

    public void setWaktuUpdateString(String waktuUpdateString) {
        this.waktuUpdateString = waktuUpdateString;
    }
}
