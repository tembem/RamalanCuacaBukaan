package gulajava.ramalancuaca.internets.modelnet.cuacaperhari;

import java.util.ArrayList;
import java.util.List;

import gulajava.ramalancuaca.internets.modelnet.cuacaperjam.CityModel;

/**
 * Created by Gulajava Ministudio.
 */
public class CuacaPerHari {

    private CityModel city;

    private List<ForecastHariModel> list = new ArrayList<>();

    public CuacaPerHari() {
    }

    public CityModel getCity() {
        return city;
    }

    public void setCity(CityModel city) {
        this.city = city;
    }

    public List<ForecastHariModel> getList() {
        return list;
    }

    public void setList(List<ForecastHariModel> list) {
        this.list = list;
    }
}
