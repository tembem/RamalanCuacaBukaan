package gulajava.ramalancuaca.internets.sinkronisasi;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.PendingIntent;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SyncRequest;
import android.content.SyncResult;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.joda.time.DateTime;

import java.util.List;
import java.util.concurrent.Callable;

import bolts.Continuation;
import bolts.Task;
import gulajava.ramalancuaca.R;
import gulajava.ramalancuaca.aktivitas.LoadingSplash;
import gulajava.ramalancuaca.database.RMCuacaHarian;
import gulajava.ramalancuaca.database.RMCuacaPerJam;
import gulajava.ramalancuaca.database.RMSetelan;
import gulajava.ramalancuaca.database.RealmKonfigurasi;
import gulajava.ramalancuaca.internets.GoogleClientBuilder;
import gulajava.ramalancuaca.internets.OkHttpSingleton;
import gulajava.ramalancuaca.internets.StatusApp;
import gulajava.ramalancuaca.internets.modelnet.cuacaperhari.CuacaPerHari;
import gulajava.ramalancuaca.internets.modelnet.cuacaperhari.ForecastHariModel;
import gulajava.ramalancuaca.internets.modelnet.cuacaperhari.TemperaturModel;
import gulajava.ramalancuaca.internets.modelnet.cuacaperjam.CuacaPerJams;
import gulajava.ramalancuaca.internets.modelnet.cuacaperjam.ForecastJamModel;
import gulajava.ramalancuaca.internets.modelnet.cuacaperjam.MainForecastModel;
import gulajava.ramalancuaca.internets.modelnet.cuacaperjam.WeatherItem;
import gulajava.ramalancuaca.internets.modelnet.cuacaperjam.WindModel;
import gulajava.ramalancuaca.internets.servisnet.RequestBuilder;
import gulajava.ramalancuaca.modellogika.Parsers;
import gulajava.ramalancuaca.models.MsgHasilGeocoder;
import gulajava.ramalancuaca.utilans.CekGPSNet;
import gulajava.ramalancuaca.utilans.Konstan;
import gulajava.ramalancuaca.utilans.UtilanCuaca;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Gulajava Ministudio.
 */
public class RCuacaSyncAdapter extends AbstractThreadedSyncAdapter {

    // Interval at which to sync with the weather, in seconds.
    // 60 seconds (1 minute) * 180 = 3 hours
    // 1 menit = 60 detik, 1 jam 60 menit, dalam 3 jam maka ada 180 menit
    // 3 jam * 60 menit * 60 detik
    // konversikan ke dalam detik
    public static final int SYNC_INTERVAL = 60 * 180;
    // ganti dengan durasi 1 jam sekali untuk pengetesan
//    public static final int SYNC_INTERVAL = 60 * 60;

    // waktu tercepat untuk melakukan sinkronisasi
    public static final int SYNC_FLEXTIME = SYNC_INTERVAL / 3;


    // ID notifikasi untuk pemberitahuan cuaca
    private static final int WEATHER_NOTIFICATION_ID = 34;


    private RealmConfiguration realmConfiguration;
    private boolean isMintaDataJamOK = false;
    private boolean isMintaDataHariOK = false;

    private Location mLocationBaru;
    private GoogleApiClient mGoogleApiClient;
    private RMSetelan mRMSetelanKopian;
    private String mStringLatitude;
    private String mStringLongitude;
    private String namaKotaPendek;
    private String namaKotaPanjang;


    public RCuacaSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
    }

    @Override
    public void onPerformSync(Account account,
                              Bundle bundle, String s,
                              ContentProviderClient contentProviderClient,
                              SyncResult syncResult) {

        Log.w("MULAI SINKRONISASI", "mulai sinkronisasi data");
        if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
            // On UI thread.
            Log.w("THREADING", "UI MAIN THREAD");
        } else {
            // Not on UI thread.
            Log.w("THREADING", "BACKGROUND THREAD");
        }

        isMintaDataJamOK = false;
        isMintaDataHariOK = false;

        boolean isSetelanAda;
        Context context = RCuacaSyncAdapter.this.getContext();
        Request requestPerJam;
        Request requestPerHari;
        OkHttpClient okHttpClient = OkHttpSingleton.getInstance().getOkHttpClient();
        Parsers parsers = new Parsers(RCuacaSyncAdapter.this.getContext());


        //ambil data lokasi dari database noSQL Realm
        realmConfiguration = RealmKonfigurasi.getKonfigurasiRealm(context);
        Realm realm = Realm.getInstance(realmConfiguration);

        RealmQuery<RMSetelan> rmSetelanRealmQuery = realm.where(RMSetelan.class);
        RealmResults<RMSetelan> rmSetelanRealmResults = rmSetelanRealmQuery.findAll();

        //jika ada setelan yang berisi data lokasi dan lainnya, lanjutkan
        if (!rmSetelanRealmResults.isEmpty()) {

            isSetelanAda = true;

            RMSetelan rmSetelan = rmSetelanRealmResults.first();
            mRMSetelanKopian = realm.copyFromRealm(rmSetelan);

            String strLatitude = rmSetelan.getStrLatitude();
            String strLongitude = rmSetelan.getStrLongitude();

            if (strLatitude.length() > 2 && strLongitude.length() > 2) {

                //susun request untuk melakukan sinkronisasi
                //request untuk ambil data per hari
                requestPerHari = RequestBuilder.buildRequestCuacaHarian(Konstan.PARAM_ISI_JUMLAH_HARI,
                        strLatitude, strLongitude);

                //request untuk ambil data per jam
                //dengan rentang cuaca setiap 3 jam sekali, sebanyak 10 kali =>
                requestPerJam = RequestBuilder.buildRequestCuacaPerJam(Konstan.PARAM_ISI_JUMLAH_JAM,
                        strLatitude, strLongitude);

                //eksekusi request OkHttp3
                try {
                    Response responseCuacaPerHari = okHttpClient.newCall(requestPerHari).execute();

                    if (responseCuacaPerHari.isSuccessful()) {

                        String dataJsonCuacaPerHari = responseCuacaPerHari.body().string();

                        //deserial data json
                        CuacaPerHari cuacaPerHari = parsers.parseJSONCuacaHarian(dataJsonCuacaPerHari);
                        deserialDataJsonCuacaHARI(realm, cuacaPerHari);
                    } else {
                        isMintaDataHariOK = false;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    isMintaDataHariOK = false;
                }

                try {
                    Response responseCuacaPerJam = okHttpClient.newCall(requestPerJam).execute();

                    if (responseCuacaPerJam.isSuccessful()) {

                        String dataJsonCuacaPerJam = responseCuacaPerJam.body().string();

                        //deserial data json
                        CuacaPerJams cuacaPerJams = parsers.parseJSONCuacaPerJAM(dataJsonCuacaPerJam);
                        deserialDataJsonCuacaJAM(realm, cuacaPerJams);
                    } else {
                        isMintaDataJamOK = false;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    isMintaDataJamOK = false;
                }
            }

        } else {
            isSetelanAda = false;
        }

        realm.close();


        // selesai sinkronisasi, coba ambil lokasi gps dari perangkat
        // jika dapat lokasi dan nama lokasi, maka lanjutkan simpan ke database lokasi pengguna
        boolean isPermisiOK = cekPermisiLokasi();
        if (isPermisiOK && isSetelanAda) {

            CekGPSNet cekGPSNet = new CekGPSNet(RCuacaSyncAdapter.this.getContext());
            boolean isInternet = cekGPSNet.cekStatusInternet();
            boolean isNetworkNyala = cekGPSNet.cekStatusNetworkGSM();
            boolean isGPSNetwork = cekGPSNet.cekStatusNetwork();

            //jika koneksi internet nyala , atau sinyal gsm nyala, dan ijin lokasi nyala
            if (isInternet && isGPSNetwork || isNetworkNyala && isGPSNetwork) {

                if (mGoogleApiClient == null) {

                    mGoogleApiClient = GoogleClientBuilder.getLocationClient(
                            RCuacaSyncAdapter.this.getContext(),
                            mConnectionCallbacksOk,
                            mOnConnectionFailedListener
                    );
                }

                jalanGoogleApiClient();
            }
        }


        //jika berhasil ambil data cuaca per jam dan per hari,
        //tampilkan notifikasi
        if (isMintaDataJamOK && isMintaDataHariOK) {

            //notifikasikan cuaca baru
            tampilNotifikasiCuaca();

            //kirim pesan ke halaman aplikasi jika jalan
            //untuk menyegarkan data
            kirimPesanAktSegarkanData();
        }
    }


    private void jalankanTaskAmbilDataCoder() {

        Task.callInBackground(new Callable<MsgHasilGeocoder>() {
            @Override
            public MsgHasilGeocoder call() throws Exception {

                Parsers parsers = new Parsers(RCuacaSyncAdapter.this.getContext());
                return parsers.ambilGeocoderPengguna(mStringLatitude, mStringLongitude);
            }
        })
                .continueWith(new Continuation<MsgHasilGeocoder, Object>() {
                    @Override
                    public Object then(Task<MsgHasilGeocoder> task) throws Exception {

                        MsgHasilGeocoder msgHasilGeocoder = task.getResult();
                        namaKotaPendek = msgHasilGeocoder.getAlamatPendek();
                        namaKotaPanjang = msgHasilGeocoder.getAlamatgabungan();

                        simpanSetelanDenganLokasi();

                        return null;
                    }
                }, Task.UI_THREAD_EXECUTOR);
    }


    private void simpanSetelanDenganLokasi() {

        if (mLocationBaru != null && mRMSetelanKopian != null) {

            mRMSetelanKopian.setIdData(Konstan.ID_BARIS_SATU_DB);
            mRMSetelanKopian.setStrLatitude(mStringLatitude);
            mRMSetelanKopian.setStrLongitude(mStringLongitude);

            //simpan jika ada nama kota nya
            if (namaKotaPendek.length() > 1) {
                mRMSetelanKopian.setNamaLokasi(namaKotaPendek);
            }

            if (namaKotaPanjang.length() > 1) {
                mRMSetelanKopian.setNamaLokasiPanjang(namaKotaPanjang);
            }

            //setel waktu terakhir sinkronisasi
            DateTime dateTime = new DateTime();
            long waktuMsSekarang = dateTime.getMillis();
            mRMSetelanKopian.setStrMsWaktuUpdate("" + waktuMsSekarang);

            //ambil data lokasi dari database noSQL Realm
            realmConfiguration = RealmKonfigurasi.getKonfigurasiRealm(RCuacaSyncAdapter.this.getContext());
            Realm realm = Realm.getInstance(realmConfiguration);

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(mRMSetelanKopian);
                }
            });

            realm.close();
        }

        matikanGoogleApiClient();
    }


    /**
     * AMBIL DATA DARI STRING JSON DAN SIMPAN KE DATABASE
     */
    private void deserialDataJsonCuacaJAM(Realm realm, CuacaPerJams cuacaPerJams) {

        if (cuacaPerJams != null) {

            isMintaDataJamOK = true;

            List<ForecastJamModel> forecastJamModelList = cuacaPerJams.getList();
            int panjangarray = forecastJamModelList.size();

            for (int i = 0; i < panjangarray; i++) {

                ForecastJamModel forecastJamModel = forecastJamModelList.get(i);

                long waktuUpdateMs = forecastJamModel.getDt() * 1000;
                String waktuUpdateStr = forecastJamModel.getDt_txt();

                MainForecastModel mainForecastModel = forecastJamModel.getMain();
                String temperatur = mainForecastModel.getTemp() + "";
                String temperaturMin = mainForecastModel.getTemp_min() + "";
                String temperaturMax = mainForecastModel.getTemp_max() + "";
                String tekananUdara = mainForecastModel.getPressure() + "";
                String seaLevel = mainForecastModel.getSea_level() + "";
                String groundLevel = mainForecastModel.getGrnd_level() + "";
                String humidity = mainForecastModel.getHumidity() + "";
                String temperaturKelvin = mainForecastModel.getTemp_kf() + "";

                List<WeatherItem> weatherItemList = forecastJamModel.getWeather();
                WeatherItem weatherItem = weatherItemList.get(0);
                String idCuaca = weatherItem.getId() + "";
                String namaCuaca = weatherItem.getMain();
                String deskripsiCuaca = weatherItem.getDescription();
                String ikonCuaca = weatherItem.getIcon();

                WindModel windModel = forecastJamModel.getWind();
                String kecepatanAngin = windModel.getSpeed() + "";
                String arahAnginDerajat = windModel.getDeg() + "";

                //buat model untuk menyimpan ke dalam database
                final RMCuacaPerJam rmCuacaPerJam = new RMCuacaPerJam();

                rmCuacaPerJam.setId_data(i);
                rmCuacaPerJam.setWaktuUpdateMs(waktuUpdateMs);
                rmCuacaPerJam.setWaktuUpdateString(waktuUpdateStr);

                rmCuacaPerJam.setTemperatur(temperatur);
                rmCuacaPerJam.setTemperaturMin(temperaturMin);
                rmCuacaPerJam.setTemperaturMax(temperaturMax);
                rmCuacaPerJam.setTekananUdara(tekananUdara);
                rmCuacaPerJam.setKetinggianPermukaanLaut(seaLevel);
                rmCuacaPerJam.setKetinggianPermukaanTanah(groundLevel);
                rmCuacaPerJam.setKelembabanUdara(humidity);
                rmCuacaPerJam.setTemperatureKelvin(temperaturKelvin);

                rmCuacaPerJam.setIdKodeCuaca(idCuaca);
                rmCuacaPerJam.setNamaCuaca(namaCuaca);
                rmCuacaPerJam.setKeteranganCuaca(deskripsiCuaca);
                rmCuacaPerJam.setKodeIkon(ikonCuaca);

                rmCuacaPerJam.setKecepatanAngin(kecepatanAngin);
                rmCuacaPerJam.setArahDerajatAngin(arahAnginDerajat);

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {

                        realm.copyToRealmOrUpdate(rmCuacaPerJam);
                    }
                });
            }
        } else {
            isMintaDataJamOK = false;
        }
    }


    private void deserialDataJsonCuacaHARI(Realm realm, CuacaPerHari cuacaPerHari) {

        if (cuacaPerHari != null) {

            isMintaDataHariOK = true;

            List<ForecastHariModel> forecastHariModelList = cuacaPerHari.getList();
            int panjangarray = forecastHariModelList.size();

            //ambil data dari model JSON hasil parse...
            for (int i = 0; i < panjangarray; i++) {

                ForecastHariModel forecastHariModel = forecastHariModelList.get(i);

                //ubah dari UTC ke GMT dengan dikalikan 1000 ms
                long longwaktuUpdate = forecastHariModel.getDt() * 1000;

                TemperaturModel temperaturModel = forecastHariModel.getTemp();
                String temperaturDay = temperaturModel.getDay() + "";
                String temperaturMin = temperaturModel.getMin() + "";
                String temperaturMax = temperaturModel.getMax() + "";
                String temperaturNight = temperaturModel.getNight() + "";
                String temperaturEve = temperaturModel.getEve() + "";
                String temperaturMorn = temperaturModel.getMorn() + "";

                String tekananUdara = forecastHariModel.getPressure() + "";
                String kelembabanUdara = forecastHariModel.getHumidity() + "";

                List<WeatherItem> weatherItemList = forecastHariModel.getWeather();
                WeatherItem weatherItem = weatherItemList.get(0);

                String idKodeCuaca = weatherItem.getId() + "";
                String namaCuaca = weatherItem.getMain();
                String keteranganCuaca = weatherItem.getDescription();
                String kodeIkon = weatherItem.getIcon();

                String kecepatanAngin = forecastHariModel.getSpeed() + "";
                String arahDerajatAngin = forecastHariModel.getDeg() + "";
                String cuacaAwan = forecastHariModel.getClouds() + "";
                String curahHujan = forecastHariModel.getRain() + "";

                //simpan data ke dalam database
                final RMCuacaHarian rmCuacaHarian = new RMCuacaHarian();

                rmCuacaHarian.setId_data(i);
                rmCuacaHarian.setLongwaktuMs(longwaktuUpdate);
//                rmCuacaHarian.setWaktuUpdatems(waktuUpdatems);

                rmCuacaHarian.setTemperaturDay(temperaturDay);
                rmCuacaHarian.setTemperaturMin(temperaturMin);
                rmCuacaHarian.setTemperaturMax(temperaturMax);
                rmCuacaHarian.setTemperaturNight(temperaturNight);
                rmCuacaHarian.setTemperaturEve(temperaturEve);
                rmCuacaHarian.setTemperaturMorn(temperaturMorn);

                rmCuacaHarian.setTekananUdara(tekananUdara);
                rmCuacaHarian.setKelembabanUdara(kelembabanUdara);

                rmCuacaHarian.setIdKodeCuaca(idKodeCuaca);
                rmCuacaHarian.setNamaCuaca(namaCuaca);
                rmCuacaHarian.setKeteranganCuaca(keteranganCuaca);
                rmCuacaHarian.setKodeIkon(kodeIkon);

                rmCuacaHarian.setKecepatanAngin(kecepatanAngin);
                rmCuacaHarian.setArahDerajatAngin(arahDerajatAngin);

                rmCuacaHarian.setCuacaAwan(cuacaAwan);
                rmCuacaHarian.setCurahHujan(curahHujan);

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {

                        realm.copyToRealmOrUpdate(rmCuacaHarian);
                    }
                });
            }
        } else {
            isMintaDataHariOK = false;
        }
    }


    /**
     * TAMPILKAN NOTIFIKASI PADA CUACA SETELAH SINKRONISASI
     **/
    private void tampilNotifikasiCuaca() {

        Context context = RCuacaSyncAdapter.this.getContext();
        boolean statusAppJalan = StatusApp.getInstance().isStatusAplikasiJalan();

        Realm realm = Realm.getInstance(realmConfiguration);
        RealmQuery<RMCuacaPerJam> realmQueryCuacaJam = realm.where(RMCuacaPerJam.class);
        RealmResults<RMCuacaPerJam> realmResultsCuacaJam = realmQueryCuacaJam.findAll();

        RealmQuery<RMSetelan> rmSetelanRealmQuery = realm.where(RMSetelan.class);
        RealmResults<RMSetelan> realmResultsSetelan = rmSetelanRealmQuery.findAll();

        RMSetelan rmSetelan = realmResultsSetelan.first();
        boolean isNotifikasiOK = rmSetelan.isNotifikasiCuaca();

        if (isNotifikasiOK && realmResultsCuacaJam.size() > 0) {

            RMCuacaPerJam rmCuacaPerJam = realmResultsCuacaJam.first();

            try {
                String namaKotaPendek = rmSetelan.getNamaLokasi();

                String kodeCuacaSekarang = rmCuacaPerJam.getIdKodeCuaca();
                String suhuUdara = rmCuacaPerJam.getTemperatur();

                String namaCuaca = UtilanCuaca.getStatusCuacaTeks(kodeCuacaSekarang);
                int kodeGambarNotifikasi = UtilanCuaca.getStatusCuacaNotifikasi(kodeCuacaSekarang);
                String suhuUdaraFormats = UtilanCuaca.formatTemperature(RCuacaSyncAdapter.this.getContext(), suhuUdara);


                //isi notifikasi
                //Ramalan Cuaca
                //Perkiraaan hujan
                //Suhu udara 30 derajat C

                //23 derajat di Bandung
                //Cuaca Hujan
                String susunanNotifikasi1;
                if (namaKotaPendek.length() > 1) {
                    susunanNotifikasi1 = suhuUdaraFormats + " C di " + namaKotaPendek;
                } else {
                    susunanNotifikasi1 = suhuUdaraFormats + " C";
                }

                String susunanNotifikasi2 = "Cuaca " + namaCuaca;

                //intent untuk notifikasi
                Intent intentNotifikasi;
                if (!statusAppJalan) {
                    intentNotifikasi = new Intent(context, LoadingSplash.class);
                    intentNotifikasi.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                } else {
                    intentNotifikasi = new Intent();
                }

                //task stack untuk pengembalian ke halaman utama jika halaman di luar halaman utama
                //sedang dibuka
                TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(context);
                taskStackBuilder.addNextIntent(intentNotifikasi);
                PendingIntent pendingIntent = taskStackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationCompat.InboxStyle inboxStyleTheme = new NotificationCompat.InboxStyle();
                inboxStyleTheme.setBigContentTitle("Ramalan Cuaca");
                inboxStyleTheme.addLine(susunanNotifikasi1);
                inboxStyleTheme.addLine(susunanNotifikasi2);


                NotificationCompat.Builder builderNotif = new NotificationCompat.Builder(context)
                        .setColor(ContextCompat.getColor(context, R.color.sunshine_blue))
                        .setSmallIcon(kodeGambarNotifikasi)
                        .setContentIntent(pendingIntent)
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setAutoCancel(true)
                        .setOngoing(false)
                        .setContentTitle(susunanNotifikasi1)
                        .setContentText(susunanNotifikasi2);

                builderNotif.setStyle(inboxStyleTheme);

                NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
                notificationManagerCompat.notify(WEATHER_NOTIFICATION_ID, builderNotif.build());

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        realm.close();
    }


    /**
     * Metode bantuan untuk penjadwalan sinkronisasi sync adapter dan eksekusinya
     */
    public static void konfigurasiPeriodeSync(Context context,
                                              int intervalSinkron,
                                              int flexTime) {

        Account account = getSyncAccount(context);
        String autoritas = context.getResources().getString(R.string.content_authority);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

            // we can enable inexact timers in our periodic sync
            SyncRequest request = new SyncRequest.Builder()
                    .syncPeriodic(intervalSinkron, flexTime)
                    .setSyncAdapter(account, autoritas)
                    .setExtras(new Bundle())
                    .build();

            ContentResolver.requestSync(request);
        } else {
            ContentResolver.addPeriodicSync(account,
                    autoritas, new Bundle(), intervalSinkron);
        }
    }


    /**
     * Helper method to have the sync adapter sync immediately
     * metode untuk melakukan sinkronisasi secara segera
     *
     * @param context The context used to access the account service
     */
    public static void syncSegera(Context context) {

        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        ContentResolver.requestSync(getSyncAccount(context),
                context.getString(R.string.content_authority), bundle);
    }


    /**
     * Helper method to get the fake account to be used with SyncAdapter, or make a new one
     * if the fake account doesn't exist yet.  If we make a new account, we call the
     * onAccountCreated method so we can initialize things.
     *
     * @param context The context used to access the account service
     * @return a fake account.
     */
    public static Account getSyncAccount(Context context) {

        // Get an instance of the Android account manager
        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);

        // Create the account type and default account
        String namaAkun = context.getResources().getString(R.string.app_name);
        String tipeAkun = context.getResources().getString(R.string.sync_account_type);
        Account account = new Account(namaAkun, tipeAkun);

        // If the password doesn't exist, the account doesn't exist
        if (accountManager.getPassword(account) == null) {

            /*
            * Add the account and account type, no password or user data
            * If successful, return the Account object, otherwise report an error.
            */
            boolean isBerhasilDibuat = accountManager.addAccountExplicitly(account, "", null);
            if (!isBerhasilDibuat) {
                return null;
            }

            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call ContentResolver.setIsSyncable(account, AUTHORITY, 1)
             * here.
             */
            onAccountCreated(account, context);
        }

        return account;
    }


    private static void onAccountCreated(Account newAccount, Context context) {

   /*
        * Since we've created an account
        * */
        RCuacaSyncAdapter.konfigurasiPeriodeSync(context,
                SYNC_INTERVAL, SYNC_FLEXTIME);

                /*
         * Without calling setSyncAutomatically, our periodic sync will not be enabled.
         */
        ContentResolver.setSyncAutomatically(newAccount,
                context.getResources().getString(R.string.content_authority), true);

        /*
         * Finally, let's do a sync to get things started
         */
        syncSegera(context);
    }


    public static void initializeSyncAdapter(Context context) {

        getSyncAccount(context);
    }


    //CEK PERMISI LOKASI UNTUK AMBIL LOKASI TERAKHIR JIKA ADA
    private boolean cekPermisiLokasi() {

        boolean isPermisiOk = false;
        try {
            int kodeFineLokasiOK = ContextCompat.checkSelfPermission(RCuacaSyncAdapter.this.getContext(), Manifest.permission.ACCESS_FINE_LOCATION);
            int kodeCoarseLokasiOK = ContextCompat.checkSelfPermission(RCuacaSyncAdapter.this.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION);

            if (kodeFineLokasiOK != PackageManager.PERMISSION_GRANTED &&
                    kodeCoarseLokasiOK != PackageManager.PERMISSION_GRANTED) {

                isPermisiOk = false;
            } else {
                isPermisiOk = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return isPermisiOk;
    }


    //KIRIM PESAN BROADCAST INTENT KE ACTIVITY UNTUK PENYEGARAN DATA
    private void kirimPesanAktSegarkanData() {
        Intent intent = new Intent();
        intent.setAction(Konstan.KODE_BROADCAST_SERVIS);
        intent.putExtra("Data Intent", Konstan.KODE_BROADCAST_SERVIS);
        RCuacaSyncAdapter.this.getContext().sendBroadcast(intent);
    }


    /**
     * ==========  GOOGLE PLAY SERVICE LOCATION ==================
     * ==========  GOOGLE PLAY SERVICE LOCATION ==================
     * ==========  GOOGLE PLAY SERVICE LOCATION ==================
     ***/

    //JALANKAN GOOGLE API CLIENT
    //MULAI API CLIENT
    public void jalanGoogleApiClient() {

        if (mGoogleApiClient != null && !mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
    }


    //MATIKAN API CLIENT LOKASI
    public void matikanGoogleApiClient() {

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }


    //LISTENER SAMBUNGAN OK KE GOOGLE PLAY SERVICE
    GoogleApiClient.ConnectionCallbacks mConnectionCallbacksOk = new GoogleApiClient.ConnectionCallbacks() {
        @Override
        public void onConnected(@Nullable Bundle bundle) {

            if (ActivityCompat.checkSelfPermission(RCuacaSyncAdapter.this.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(RCuacaSyncAdapter.this.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }

            mLocationBaru = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLocationBaru != null) {
                mStringLatitude = mLocationBaru.getLatitude() + "";
                mStringLongitude = mLocationBaru.getLongitude() + "";
            }

            //ambil lokasi baru dengan thread asynctask
            jalankanTaskAmbilDataCoder();
        }

        @Override
        public void onConnectionSuspended(int i) {
            mGoogleApiClient.connect();
        }
    };


    //LISTENER SAMBUNGAN GAGAL KE GOOGLE PLAY SERVICE
    GoogleApiClient.OnConnectionFailedListener mOnConnectionFailedListener = new GoogleApiClient.OnConnectionFailedListener() {
        @Override
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            //coba sambungan terus google api client
            if (!mGoogleApiClient.isConnecting() && !mGoogleApiClient.isConnected()) {
                mGoogleApiClient.connect();
            }
        }
    };
}
