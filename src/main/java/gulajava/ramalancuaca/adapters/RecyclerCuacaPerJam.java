package gulajava.ramalancuaca.adapters;

import android.content.Context;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import gulajava.ramalancuaca.R;
import gulajava.ramalancuaca.database.RMCuacaPerJam;
import gulajava.ramalancuaca.utilans.UtilanCuaca;
import io.realm.RealmResults;

/**
 * Created by Gulajava Ministudio.
 */
public class RecyclerCuacaPerJam extends RecyclerView.Adapter<ViewHolder> {

    private Context mContext;
    private RealmResults<RMCuacaPerJam> mCuacaPerJamRealmResults;

    private final TypedValue typedvalues = new TypedValue();
    private int mBackground;
    private OnItemClickListener onItemClickListener;

    private static final int TIPE_VIEW_HEADER_SEKARANG = 10;
    private static final int TIPE_VIEW_ISI_SEKARANG = 11;
    private static final int TIPE_VIEW_HEADER_JAM_BERIKUT = 12;
    private static final int TIPE_VIEW_ISI_JAM_BERIKUT = 13;
    private static final int TIPE_VIEW_ISI_KOSONG = 14;

    private int PANJANG_HEADER_TANGGAL = 1;
    private int PANJANG_HEADER_JAMBERIKUT = 1;
    private int mIntPanjangData = 0;


    public RecyclerCuacaPerJam(Context context, RealmResults<RMCuacaPerJam> cuacaPerJamRealmResults) {

        mContext = context;
        mCuacaPerJamRealmResults = cuacaPerJamRealmResults;
        mIntPanjangData = mCuacaPerJamRealmResults.size();

        //setel agar recycler view ada latar belakangnya
        mContext.getTheme().resolveAttribute(android.R.attr.selectableItemBackground, typedvalues, true);
        mBackground = typedvalues.resourceId;
    }


    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);

        int tipeview;

        if (mIntPanjangData > 0) {

            switch (position) {

                case 0:

                    tipeview = TIPE_VIEW_HEADER_SEKARANG;
                    break;

                case 1:

                    tipeview = TIPE_VIEW_ISI_SEKARANG;
                    break;

                case 2:

                    tipeview = TIPE_VIEW_HEADER_JAM_BERIKUT;
                    break;

                default:

                    tipeview = TIPE_VIEW_ISI_JAM_BERIKUT;
                    break;
            }

        } else {
            tipeview = TIPE_VIEW_ISI_KOSONG;
        }

        return tipeview;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;

        switch (viewType) {

            case TIPE_VIEW_ISI_KOSONG:

                view = LayoutInflater.from(mContext).inflate(R.layout.desainbaris_kondisikosong, parent, false);
                return new ViewHolderDataKosong(view);

            case TIPE_VIEW_HEADER_SEKARANG:

                view = LayoutInflater.from(mContext).inflate(R.layout.desainbaris_header_cuacasekarang, parent, false);
                view.setBackgroundColor(ContextCompat.getColor(mContext, R.color.transparan));
                return new ViewHolderTanggal(view);

            case TIPE_VIEW_ISI_SEKARANG:

                view = LayoutInflater.from(mContext).inflate(R.layout.desainbaris_cuaca_sekarang, parent, false);
                view.setBackgroundColor(ContextCompat.getColor(mContext, R.color.transparan));
                return new ViewHolderCuacaSekarang(view);

            case TIPE_VIEW_HEADER_JAM_BERIKUT:

                view = LayoutInflater.from(mContext).inflate(R.layout.desainbaris_header_cuaca_jamberikut, parent, false);
                view.setBackgroundColor(ContextCompat.getColor(mContext, R.color.transparan));
                return new ViewHolderHeader(view);

            default:

                view = LayoutInflater.from(mContext).inflate(R.layout.desainbaris_cuaca_perjam, parent, false);
                view.setBackgroundResource(mBackground);
                return new ViewHolderCuacaJamBerikut(view);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        int tipeView = getItemViewType(position);
        RMCuacaPerJam rmCuacaPerJam;

        switch (tipeView) {

            case TIPE_VIEW_HEADER_SEKARANG:

                //ambil data dari baris pertama
                rmCuacaPerJam = mCuacaPerJamRealmResults.get(position);
                long tanggalSekarangMs = rmCuacaPerJam.getWaktuUpdateMs();
                String tanggalSekarangFormat = UtilanCuaca.konversiMsTanggal(tanggalSekarangMs);
                ViewHolderTanggal viewHolderTanggal = (ViewHolderTanggal) holder;
                viewHolderTanggal.mTextViewCuacaSekarang.setText(tanggalSekarangFormat);

                break;

            case TIPE_VIEW_ISI_SEKARANG:

                //ambil data dari baris pertama juga
                final int posisiList_isiSekarang = position - 1;
                rmCuacaPerJam = mCuacaPerJamRealmResults.get(posisiList_isiSekarang);

                final int idBarisDB = rmCuacaPerJam.getId_data();
                String strkodeCuaca = rmCuacaPerJam.getIdKodeCuaca();
                String temperaturMax = rmCuacaPerJam.getTemperaturMax();
                String temperaturMin = rmCuacaPerJam.getTemperaturMin();

                String namaCuacaFormat = UtilanCuaca.getStatusCuacaTeks(strkodeCuaca);
                int kodeGambarCuaca = UtilanCuaca.getGambarCuacaDariKodeKondisi(strkodeCuaca);
                String temperaturMaxFormat = UtilanCuaca.formatTemperature(mContext, temperaturMax);
                String temperaturMinFormat = UtilanCuaca.formatTemperature(mContext, temperaturMin);

                ViewHolderCuacaSekarang viewHolderCuacaSekarang = (ViewHolderCuacaSekarang) holder;
                viewHolderCuacaSekarang.mTextViewSuhuMin.setText(temperaturMinFormat);
                viewHolderCuacaSekarang.mTextViewSuhuMaks.setText(temperaturMaxFormat);
                viewHolderCuacaSekarang.mTextViewNamaCuaca.setText(namaCuacaFormat);

                Glide.with(mContext).load(kodeGambarCuaca).placeholder(R.drawable.weather_none_available)
                        .error(R.drawable.weather_none_available)
                        .into(viewHolderCuacaSekarang.mImageViewGambarCuaca);

                viewHolderCuacaSekarang.mViewContainer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (onItemClickListener != null) {

                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    onItemClickListener.onItemClick(posisiList_isiSekarang, idBarisDB);
                                }
                            }, 250);
                        }
                    }
                });

                break;

            case TIPE_VIEW_ISI_JAM_BERIKUT:

                //daftar baris untuk isi jam berikutnya
                ViewHolderCuacaJamBerikut viewHolderCuacaJamBerikut = (ViewHolderCuacaJamBerikut) holder;

                final int posisiList_jamberikut = position - 2;
                rmCuacaPerJam = mCuacaPerJamRealmResults.get(posisiList_jamberikut);
                final int idBarisDB_berikut = rmCuacaPerJam.getId_data();
                long strJamBerikutMs = rmCuacaPerJam.getWaktuUpdateMs();
                String strtemperaturMin = rmCuacaPerJam.getTemperaturMin();
                String strtemperaturMax = rmCuacaPerJam.getTemperaturMax();
                String strKodeCuaca = rmCuacaPerJam.getIdKodeCuaca();

                String jamberikutFormat = UtilanCuaca.konversiMsJam(strJamBerikutMs);
                String tempMinFormat = UtilanCuaca.formatTemperature(mContext, strtemperaturMin);
                String tempMaxFormat = UtilanCuaca.formatTemperature(mContext, strtemperaturMax);
                int kodegambarCuaca = UtilanCuaca.getGambarCuacaDariKodeKondisi(strKodeCuaca);

                viewHolderCuacaJamBerikut.mTextViewJam.setText(jamberikutFormat);
                viewHolderCuacaJamBerikut.mTextViewSuhuMaks.setText(tempMaxFormat);
                viewHolderCuacaJamBerikut.mTextViewSuhuMin.setText(tempMinFormat);

                Glide.with(mContext).load(kodegambarCuaca)
                        .error(R.drawable.weather_none_available)
                        .placeholder(R.drawable.weather_none_available)
                        .into(viewHolderCuacaJamBerikut.mImageViewGambarCuaca);

                viewHolderCuacaJamBerikut.mViewContainer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (onItemClickListener != null) {

                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    onItemClickListener.onItemClick(posisiList_jamberikut, idBarisDB_berikut);
                                }
                            }, 250);
                        }
                    }
                });

                break;
        }
    }

    @Override
    public int getItemCount() {

        //jika panjang data lebih besar dari 0, balikkan nilai data
        //jika panjang data 0, balikkan nilai 1 agar viewholder status kosong bisa tampil
        if (mIntPanjangData > 0) {

            return mIntPanjangData + PANJANG_HEADER_TANGGAL + PANJANG_HEADER_JAMBERIKUT;
        } else {
            return 1;
        }
    }


    //DATA KOSONG
    protected class ViewHolderDataKosong extends ViewHolder {

        public View mViewContainer;

        public ViewHolderDataKosong(View itemView) {
            super(itemView);
            mViewContainer = itemView;
        }
    }


    //HEADER DATA BERBENTUK TANGGAL
    protected class ViewHolderTanggal extends ViewHolder {

        public View mViewContainer;
        public TextView mTextViewCuacaSekarang;

        public ViewHolderTanggal(View itemView) {
            super(itemView);
            mViewContainer = itemView;

            mTextViewCuacaSekarang = (TextView) itemView.findViewById(R.id.teks_header_tanggalsekarang);
        }
    }


    //ISI CUACA DATA JAM SEKARANG
    protected class ViewHolderCuacaSekarang extends ViewHolder {

        public View mViewContainer;

        public TextView mTextViewNamaCuaca;
        public TextView mTextViewSuhuMaks;
        public TextView mTextViewSuhuMin;
        public ImageView mImageViewGambarCuaca;

        public ViewHolderCuacaSekarang(View itemView) {
            super(itemView);
            mViewContainer = itemView;

            mTextViewNamaCuaca = (TextView) itemView.findViewById(R.id.teks_nama_cuaca);
            mTextViewSuhuMaks = (TextView) itemView.findViewById(R.id.teks_temperatur_maks);
            mTextViewSuhuMin = (TextView) itemView.findViewById(R.id.teks_temperatur_minimum);

            mImageViewGambarCuaca = (ImageView) itemView.findViewById(R.id.gambar_kondisicuaca_list);
        }
    }


    //ISI CUACA DATA JAM BERIKUT
    protected class ViewHolderCuacaJamBerikut extends ViewHolder {

        private TextView mTextViewJam;
        private TextView mTextViewSuhuMaks;
        private TextView mTextViewSuhuMin;
        private ImageView mImageViewGambarCuaca;

        public View mViewContainer;

        public ViewHolderCuacaJamBerikut(View itemView) {
            super(itemView);
            mViewContainer = itemView;

            mImageViewGambarCuaca = (ImageView) itemView.findViewById(R.id.gambar_kondisicuaca);
            mTextViewJam = (TextView) itemView.findViewById(R.id.teks_jam);
            mTextViewSuhuMaks = (TextView) itemView.findViewById(R.id.teks_suhu_tinggi);
            mTextViewSuhuMin = (TextView) itemView.findViewById(R.id.teks_suhu_rendah);
        }
    }


    protected class ViewHolderHeader extends ViewHolder {

        public View mViewContainer;

        public ViewHolderHeader(View itemView) {
            super(itemView);
            mViewContainer = itemView;
        }
    }


    //METODE SEGARKAN REALM
    public void segarkanDataRealm() {

        mIntPanjangData = mCuacaPerJamRealmResults.size();
        RecyclerCuacaPerJam.this.notifyDataSetChanged();
    }


    //METODE INTERFACE JIKA DAFTAR DI KLIK
    public interface OnItemClickListener {
        void onItemClick(int posisiKlik, int idDatabase);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
