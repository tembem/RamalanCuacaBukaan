# Ramalan Cuaca (Project Sunshine X)

Aplikasi sederhana untuk menampilkan daftar perkiraan cuaca dalam satu hari dan beberapa hari berikutnya. Aplikasi ini menggunakan data cuaca dari Open Weather dan ditampilkan ke dalam bentuk yang sederhana dan mudah dilihat.

Aplikasi akan melakukan sinkronisasi otomatis ke server cuaca setiap satu jam sekali. Sinkronisasi bisa dilakukan jika setelan sinkronisasi di perangkat sedang aktif.

Aplikasi membutuhkan koneksi internet dan GPS untuk mengambil posisi perangkat sehingga data cuaca yang diambil bisa sesuai dengan lokasi pengguna tersebut berada. Sehingga pastikan koneksi internet dan akses GPS anda aktif agar data cuaca yang didapat lebih akurat.

Aplikasi dibuat berdasarkan panduan dari Project Sunshine yang tersedia di pelatihan kelas online Udacity level Intermediate. Namun dengan sedikit polesan dan perbaikan. Kode sumber aplikasi ini bisa dilihat di Gitlab di :

https://gitlab.com/gulajava.mini/RamalanCuacaBukaan

Untuk Project Sunshine nya, bisa dilihat di [Udacity Google] untuk level [Intermediate]





Aplikasi ini menggunakan beberapa library yang mana instalasi dan pemasangannya perlu diliat ke situs library tersebut, Yaitu : 
  - Database [Realm] untuk simpan data cuaca
  - [OkHttp] untuk transfer data
  - [Google Play Service Location] untuk deteksi otomatis lokasi perangkat
  - [FastJSON] untuk deserialisasi data JSON yang diperoleh

Aplikasi ini menggunakan API Open Weather. Yang mana pendaftaran untuk mendapatkan API Key nya dapat dilihat di :

http://openweathermap.org/api

https://github.com/udacity/Sunshine-Version-2



Demo penampakan aplikasi ini dapat diunduh di Google Play dengan tautan berikut ini :

https://play.google.com/store/apps/details?id=gulajava.ramalancuaca




   [Realm]: <https://realm.io/>
   [OkHttp]: <http://square.github.io/okhttp/>
   [Google Play Service Location]: <http://developer.android.com/training/location/index.html>
   [Udacity Google]: <https://www.udacity.com/google>
   [Intermediate]: <https://www.udacity.com/course/developing-android-apps--ud853>
   [FastJSON]: <https://github.com/alibaba/fastjson>
   
   